#!/usr/bin/env python
# This is code is designed to download list of known bad IPs and domains
# Once the lists have been downloaded, 2 reference sets are created
# 1 for IPs and 1 for domains
# Manual creation of QRadar rules are then done. These rules are then run
# against these list to identify known bad IPs and Domain
#
# SecurityNikThreatIntel.py v1.0
# Author: Nik Alleyne, CISSP|GCIH|A < nikalleyne@gmail.com >
# Date: 2015-02-25
# Disclaimer: In no way am I responsible for any damages which you may
# cause to your system by running this script.

from os import uname, path, system, remove, getcwd
from shutil import rmtree, copytree
from subprocess import call
from sys import exit
from time import sleep
from ConfigParser import ConfigParser
import json

# set up config object
config = ConfigParser()
config.read('setup.cfg')

# This function checks to see if this script is running on Linux.


def check_os():
    """
    Checks the OS and makes sure it's Linux
    :return:
    """
    qRadar_path = config.get('fileLocations', 'qRadar_path')
    qRadar_ver = config.get('fileLocations', 'qRadar_ver')

    print(' Checking OS ... ')
    if (uname()[0] == 'Linux') or (uname()[0] == 'linux'):
        # print(' Running on Linux ... ')

        if all([path.exists('/etc/system-release'),
                path.isfile('/etc/system-release')]):
            call(['cat', '/etc/system-release'])
        else:
            print('\n Looks like you are running Linux. ')
            print('\n However, I am unable to determine your version info. ')

        print(' \n Looking for an installed version of QRadar')
        if path.exists(qRadar_path) and (path.isdir(qRadar_path)):
            print(' \n looks like you are running QRadar version ... ')
            call([qRadar_ver])
            print(' \n Good stuff ... \n Blast off =>>>>>>> ')
        else:
            print(
                ' An installed version of QRadar was not found on your system ')
            print(
                ' This script will not work for you, it was designed to be used on box running IBM QRadar ')
            print(' Exiting ... ')
            exit(0)

        sleep(2)
    else:
        print(' Running this is a waste of your time. ')
        print(' This script is SPECIFICALLY for QRadar ')
        exit(0)


def grab_ip_list():
    """
    This function downloads a list of known bad IPs and ?
    :return:
    """
    ip_path = config.get('fileLocations', 'ip_first_tmp_path')
    ip_alt_path = config.get('fileLocations', 'ip_other_tmp_path')
    bad_ip_list = json.loads(config.get('jsonListURLs', 'IPs'))

    # Check to see if ip_tmp/ folder exists - This folder stores the files a the first download.
    # Basically this will determine if its the first time the script is being
    # run
    if path.exists(ip_path) and path.isdir(ip_path):
        ip_path = ip_alt_path

    try:
        print(' Preparing to download list of bad IP addresses ')
        for link in bad_ip_list:
            print(link)
            call(['wget', link, '--directory-prefix=' + ip_path, '--tries=2', '--continue',
                  '--timestamping', '--timeout=5', '--random-wait', '--no-proxy', '--inet4-only'])
            print(' \n  %s \n retrieved successfully \n' % link)
            sleep(2)
    except:
        print(' A problem occurred while downloading IP information from %s ' % link)
        print(' This link may be broken. Please copy the URL and paste into a browser to ensure it is accessible')
    else:
        # Looks like all went well
        print(' \n Looks like we have some baddddd IPs! ')


def grab_dns_list():
    """
    This fuction download the list of malicious and or suspected domains
    DO NOT add entry to this list unless you are sure what you are doing
    These files are in different formats, thus may need to be manipulated
    the files individually
    :return:
    """
    dns_path = config.get('fileLocations', 'dns_first_tmp_path')
    dns_alt_path = config.get('fileLocations', 'dns_other_tmp_path')
    bad_dns_list = json.loads(config.get('jsonListURLs', 'domains'))

    if path.exists(dns_path) and (path.isdir(dns_path)):
        dns_path = dns_alt_path

    try:
        print(' Preparing to download list of bad Domain  ')
        for dns in bad_dns_list:
            print(dns)
            call(['wget', dns, '--directory-prefix=' + dns_path, '--tries=2', '--continue',
                  '--timestamping', '--timeout=5', '--random-wait', '--no-proxy', '--inet4-only'])
            print(' \n  %s \n retrieved successfully \n' % dns)
            sleep(2)
    except:
        print(' A problem occurred while downloading DNS information from %s ' % dns)
        print(' This link may be broken. Please copy the URL and paste into a browser to ensure it is accessible')
    else:
        # Looks like all went well
        print(' \n Looks like we have some baddddd domains! ')


def compare_ip_dirs():
    """
    Checking the directories to see if the last run added new info
    :return:
    """
    print(' Checking if there is need for an update .... ')

    # first check to see if .ip_tmp_path exists
    ip_alt_path = config.get('fileLocations', 'ip_other_tmp_path')
    ip_path = config.get('fileLocations', 'ip_first_tmp_path')

    if path.exists(ip_alt_path) and (path.isdir(ip_alt_path)):
        print(' Give me just a few seconds more')
        sleep(2)

        if int(path.getsize(ip_path)) <= int(path.getsize(ip_alt_path)):
            print(' \n Looks like new content is available ')
            # copying new content in .ip_tmp_path to .ip_tmp
            try:
                rmtree(ip_path)
                copytree(ip_alt_path, ip_path)
            except:
                print(' Failed to copy new data ... ')
                print(' Exiting ... ')
                exit(0)
            else:
                print(' Successfully moved new data')
        else:
            print(' Nothing new was added ... ')
            print(' Exiting ... ')
            exit(0)
    else:
        print(' This is first run ... \n moving on ... ')

    sleep(2)


def compare_dns_dirs():
    """
    Comparing the DNS folders to see if new content may have been added
    :return:
    """
    print(' Checking if there is need for an update .... ')
    dns_path = config.get('fileLocations', 'dns_first_tmp_path')
    dns_alt_path = config.get('fileLocations', 'dns_other_tmp_path')

    # first check to see if .ip_tmp_path exists
    if path.exists(dns_alt_path) and (path.isdir(dns_alt_path)):
        print(' Give me just a few seconds more')
        sleep(2)

        if int(path.getsize(dns_path)) <= int(path.getsize(dns_alt_path)):
            print(' \n Looks like new content is available ')

            # copying new content in .dns_tmp_path to .dns_tmp
            try:
                rmtree(dns_path)
                copytree(dns_alt_path, dns_path)
            except:
                print(' Failed to copy new data ... ')
                print(' Exiting ... ')
                exit(0)
            else:
                print(' Successfully moved new data')
        else:
            print(' Nothing new was added ... ')
            print(' Exiting ... ')
            exit(0)
    else:
        print(' This is first run ... \n moving on ... ')
        sleep(2)


def combine_ip_files():
    """
    Now that the files have been successfully downloaded, let's combine them all
    :return:
    """
    print(' \n Checking for .ip_tmp folder ... ')
    sleep(2)

    ip_path = config.get('fileLocations', 'ip_first_tmp_path')
    ip_file = config.get('fileLocations', 'ip_storage_file')

    if path.exists(ip_path) and path.isdir(ip_path):
        print(' directory %s found ' % ip_path)
        system(
            'cat %s* | '
            'grep --perl-regexp --only-matching "\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}" | '
            'sort -i | '
            'uniq --unique --check-chars=15 > %s' %
            (ip_path, ip_file)
        )

        if path.exists(ip_file) and path.isfile(ip_file):
            print(' Successfully created file %s ' % ip_file)
        else:
            print(' Unable to create %s file ' % ip_file)
            print(' The program will now exit ... Exiting ... ')
            exit(0)
    else:
        print(' \n %s directory not found ' % ip_path)
        print(' Unable to continue ... Exiting!')
        exit(0)


def combine_dns_files():
    """
    This function manipulates the downloaded DNS files, so that all can be
    placed into one standard file
    :return:
    """
    print(' Combining DNS files ')
    dns_path = config.get('fileLocations', 'dns_first_tmp_path')
    dns_storage_file = config.get('fileLocations', 'dns_storage_file')

    if path.exists(dns_path) and path.isdir(dns_path):
        print(' directory % found ' % dns_path)
        try:
            print(' Combining downloaded files into .... ')
            # here is where we need to do some testing of the files
            system('cat .%s/dom-bl.txt > .%s' % (dns_path, dns_storage_file))
            system('cat .%s/dom-bl-base.txt >> .%s' %
                   (dns_path, dns_storage_file))
            system("cat .%s/hosts.txt | awk  '/127.0.0.1/ { print $2 }'  >> .%s" % (dns_path, dns_storage_file))
            system('cat .%s/immortal_domains.txt | grep -i -P "This is a list|^$" -v >> .%s' %
                   (dns_path, dns_storage_file))
            system('cat .%s/BOOT | grep -i PRIMARY | cut -f 2 -d " " | grep -i -v -P "ibm\.com" -v >> .%s' %
                   (dns_path, dns_storage_file))
            system('cat .%s/dynamic_dns.txt | grep -P -v "^#|^$" | cut -f 1 -s >> .%s' %
                   (dns_path, dns_storage_file))
            system('cat .%s/blocklist.php\?download\=baddomains | grep -P -v "^#|^$" >> .%s' %
                   (dns_path, dns_storage_file))
            system('cat .%s | sort -i | uniq --unique > %s' %
                   (dns_storage_file, dns_storage_file))

        except:
            print(' Looks like an error occurred while combining the files')
            print(' Please retry later ... \n Exiting ... ')
            exit(0)
        else:
            print(' files successfully combined ')
            print(' A list of known bad domains can be found in %s' %
                  dns_storage_file)
            remove('.%s' % dns_storage_file)

    else:
        print(' \n %s directory not found ' % dns_path)
        print(' The program will now exit ... Exiting ... ')
        exit(0)


def verify_create_ip_reference_set():
    """
    This function does all the work for the IP reference set
    :return:
    """
    ip_file = config.get('fileLocations', 'ip_storage_file')
    reference_set_name = config.get('fileLocations', 'reference_set_name')
    count_file = config.get('fileLocations', 'count_file')
    ip_txt = getcwd() + '/%s' % ip_file
    rows = []

    print('Checking to see if the reference set %s already exists' %
          reference_set_name)
    f = open('.count.txt', 'w')
    call(["psql", "-U", "qradar", "--command=SELECT COUNT(*) FROM reference_data WHERE name='%s'" %
          reference_set_name], stdout=f)
    f.close()

    # Resting ... I'm tired
    sleep(2)

    f = open('.%s' % count_file, 'r')

    for line in f.readlines():
        rows.append(line.strip())
    # print(rows)

    if rows[2].strip() != '0':
        print(' Looks like reference set already exists \n ')
    else:
        print(' Reference Set %s not found ...  %reference_set_name ')
        print(' Looks like we will have to create this bad boy ...')

        try:
            call(
                ['/opt/qradar/bin/ReferenceSetUtil.sh', 'create', reference_set_name, 'IP'])
            print(' Successfully created reference set %s \n ' %
                  reference_set_name)
            # print(' Looks like that went well ... ' )
        except:
            # This does not catch any java exception that may be created
            print(' Error occurred while creating reference set %s ' %
                  reference_set_name)
            print(' You may create the reference set %s manually if needed ' %
                  reference_set_name)
            exit(0)

    print(' Loading information into reference set %s ' % reference_set_name)
    try:
        call(
            ['/opt/qradar/bin/ReferenceSetUtil.sh', 'load', reference_set_name, ip_txt])
        print(' \n You may need to verify that you have rules created to use %s ' %
              reference_set_name)
    except:
        print(' An error occurred while loading the reference set ... ')
        print(' Please retry later!')
        exit(0)
    remove('.%s' % count_file)


def verify_create_dns_reference_set():
    """
    This function creates the DNS reference set
    :return:
    """
    reference_set_name = config.get('fileLocations', 'reference_dns_set_name')
    dns_storage_file = config.get('fileLocations', 'dns_storage_file')
    count_file = config.get('fileLocations', 'count_file')
    dns_txt = getcwd() + '/%s' % dns_storage_file
    dns_rows = []

    print('Checking to see if the reference set %s already exists' %
          reference_set_name)
    f = open('.%s' % count_file, 'w')
    call(["psql", "-U", "qradar", "--command=SELECT COUNT(*) FROM reference_data WHERE name='%s'" %
          reference_set_name], stdout=f)
    f.close()

    # Taking a nap ...
    sleep(2)

    f = open('.%s' % count_file, 'r')
    for line in f.readlines():
        dns_rows.append(line.strip())
    # print(dns_rows)

    if dns_rows[2].strip() != '0':
        print(' Looks like reference set already exists \n ')
    else:
        print(' Reference Set %s not found ' % reference_set_name)
        print(' Looks like we will have to create this bad boy ...')
        try:
            call(
                ['/opt/qradar/bin/ReferenceSetUtil.sh', 'create', reference_set_name, 'ALN'])
            print(' Successfully created reference set %s ' %
                  reference_set_name)

            # print(' Looks like that went well ... ' )
        except:
            # This does not catch any java exception that may be created
            print(' Error occurred while creating reference set %s ' % reference_set_name)
            print(' You may create the reference set %s manually if needed ' %
                  reference_set_name)
            exit(0)

    print(' Loading information into reference set %s ' % reference_set_name)

    try:
        call(
            ['/opt/qradar/bin/ReferenceSetUtil.sh', 'load', reference_set_name, dns_txt])
        print(' \n You may need to verify that you have rules created to use %s ' %
              reference_set_name)
    except:
        print(' An error occurred while loading the reference set ... ')
        print(' Please retry later!')
        exit(0)
    remove('.%s' % count_file)


# Main Function
def main():
    call('clear')
    check_os()

    # Let's work on the IP Reference Set
    grab_ip_list()
    compare_ip_dirs()
    combine_ip_files()
    verify_create_ip_reference_set()

    # Let's work on the DNS Reference Set
    grab_dns_list()
    compare_dns_dirs()
    combine_dns_files()
    verify_create_dns_reference_set()


if __name__ == "__main__":
    main()
